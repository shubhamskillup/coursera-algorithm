# Overview

There are python exercises given in every week of the course for your practice. We are using **Jupyter Notebook** to run these files.

## How to save the Files

Download the file by clicking on it and saving it in your working directory. You can upload the files in the notebook where you can open them.

> Note: In case you are not seeing the file type as **python** while saving it, I would suggest you add .py in the file name and select **All Files** as type.

## How to run the Files

The files are given in the .py extension which can't be run directly, to run these files you can use one of the below-given options.

1. You can open a new python notebook by clicking on the **New** button given in the top right of the Jupyter home page and select the **Python3** option. It will open a new notebook where you can paste your python codes and run it(Click **shift**+**enter** to run the code cells).

2. You can also change the .py extension to .ipynb that is required to run the codes in the notebook. To change your python file into the Jupyter notebook file, please click on the **New** button again and then select the Terminal option. Now run the below python script.

    `p2j myscript.py`

> Note: Please replace _myscript.py_ with your python file which you want to change.

This will create a **myscript.ipynb** file in the same Notebook directory where the python file is present. Open the file and run the python codes.










